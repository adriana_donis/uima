package com.cs250.joanne.myfragments;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Adriana on 3/26/2017.
 */

public class Lesson {

    private Date date;
    private double hours;
    private String timeDay;
    private String type;
    private String weather;

    SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

    public Lesson(Date d, double h, String td, String t, String w) {
        this.date = d;
        this.hours = h;
        this.timeDay = td;
        this.type = t;
        this.weather = w;
    }

    public String getDateString() {
        return dt.format(date);
    }

    public Date getDate() {
        return date;
    }

    public double getHours() {
        return hours;
    }

    public String getTimeDay() {
        return timeDay;
    }

    public String getType() {
        return type;
    }

    public String getWeather() {
        return weather;
    }
}
