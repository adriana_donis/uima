package com.cs250.joanne.myfragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class Statistics extends Fragment {

    ArrayList<TextView> textViews = new ArrayList<>();
    String[] settings = {"daySettings", "nightSettings", "totalSettings", "residentialSettings",
            "commercialSettings", "highwaySettings", "sunnySettings", "rainySettings", "snowSettings"};
    int[] defaults = {10, 10, 20, 8, 5, 7, 8, 10, 2};
    int[] hardcoded = {5, 6, 11, 4, 1, 6, 7, 3, 1};
    String[] titles = {"Day", "Night", "Total", "Residential", "Commercial", "Highway", "Clear",
                        "Rainy", "Snowy"};
    SharedPreferences sharedPref;

    TextView dayTv, nightTv, totalTv, resTv, comTv, highwayTv, sunnyTv, rainyTv, snowyTv;
    ProgressBar dayP, nightP, totalP, resP, comP, highP, sunnyP, rainyP, snowP;

    ArrayList<ProgressBar> progBars = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.stats_fragment, container, false);
        sharedPref = getActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE);
        ((MainActivity) getActivity())
                .setActionBarTitle("Statistics");

        initializeTextViews(view);
        initializeProgressBars(view);

        // Inflate the layout for this fragment
        return view;
    }

    public void initializeTextViews(View view) {
        dayTv = (TextView) view.findViewById(R.id.dayTv);
        textViews.add(dayTv);
        nightTv = (TextView) view.findViewById(R.id.nightTv);
        textViews.add(nightTv);
        totalTv = (TextView) view.findViewById(R.id.totalTv);
        textViews.add(totalTv);
        resTv = (TextView) view.findViewById(R.id.residentialTv);
        textViews.add(resTv);
        comTv = (TextView) view.findViewById(R.id.commercialTv);
        textViews.add(comTv);
        highwayTv = (TextView) view.findViewById(R.id.highwayTv);
        textViews.add(highwayTv);
        sunnyTv = (TextView) view.findViewById(R.id.sunnyTv);
        textViews.add(sunnyTv);
        rainyTv = (TextView) view.findViewById(R.id.rainyTv);
        textViews.add(rainyTv);
        snowyTv = (TextView) view.findViewById(R.id.snowyTv);
        textViews.add(snowyTv);

    }

    public void initializeProgressBars(View view) {
        dayP = (ProgressBar) view.findViewById(R.id.dayPB);
        progBars.add(dayP);
        nightP = (ProgressBar) view.findViewById(R.id.nightPB);
        progBars.add(nightP);
        totalP = (ProgressBar) view.findViewById(R.id.totalPB);
        progBars.add(totalP);
        resP = (ProgressBar) view.findViewById(R.id.residentialPB);
        progBars.add(resP);
        comP = (ProgressBar) view.findViewById(R.id.commercialPB);
        progBars.add(comP);
        highP = (ProgressBar) view.findViewById(R.id.highwayPB);
        progBars.add(highP);
        sunnyP = (ProgressBar) view.findViewById(R.id.sunnyPB);
        progBars.add(sunnyP);
        rainyP = (ProgressBar) view.findViewById(R.id.rainyPB);
        progBars.add(rainyP);
        snowP = (ProgressBar) view.findViewById(R.id.snowyPB);
        progBars.add(snowP);

    }

    // Called at the start of the visible lifetime.
    @Override
    public void onStart(){
        super.onStart();
        Log.d ("Content Fragment", "onStart");
        // Apply any required UI change now that the Fragment is visible.
    }

    // Called at the start of the active lifetime.
    @Override
    public void onResume(){
        super.onResume();
        Log.d ("Content Fragment", "onResume");
        for (int j = 0; j < 9; j++) {
            String text = String.format("%s: %d/%d", titles[j], hardcoded[j], sharedPref.getInt(settings[j], defaults[j]));
            textViews.get(j).setText(text);
            progBars.get(j).setProgress(hardcoded[j] * 100 / sharedPref.getInt(settings[j], defaults[j]));
        }

        // Resume any paused UI updates, threads, or processes required
        // by the Fragment but suspended when it became inactive.
    }

    // Called at the end of the active lifetime.
    @Override
    public void onPause(){
        Log.d ("Content Fragment", "onPause");
        // Suspend UI updates, threads, or CPU intensive processes
        // that don't need to be updated when the Activity isn't
        // the active foreground activity.
        // Persist all edits or state changes
        // as after this call the process is likely to be killed.
        super.onPause();
    }

    // Called to save UI state changes at the
    // end of the active lifecycle.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d ("Content Fragment", "onSaveInstanceState");
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate, onCreateView, and
        // onCreateView if the parent Activity is killed and restarted.
        super.onSaveInstanceState(savedInstanceState);
    }

    // Called at the end of the visible lifetime.
    @Override
    public void onStop(){
        Log.d ("Content Fragment", "onStop");
        // Suspend remaining UI updates, threads, or processing
        // that aren't required when the Fragment isn't visible.
        super.onStop();
    }

    // Called when the Fragment's View has been detached.
    @Override
    public void onDestroyView() {
        Log.d ("Content Fragment", "onDestroyView");
        // Clean up resources related to the View.
        super.onDestroyView();
    }

    // Called at the end of the full lifetime.
    @Override
    public void onDestroy(){
        Log.d ("Content Fragment", "onDestroy");
        // Clean up any resources including ending threads,
        // closing database connections etc.
        super.onDestroy();
    }

    // Called when the Fragment has been detached from its parent Activity.
    @Override
    public void onDetach() {
        Log.d ("Content Fragment", "onDetach");
        super.onDetach();
    }
}
