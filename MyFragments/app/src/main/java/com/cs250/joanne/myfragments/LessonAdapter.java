package com.cs250.joanne.myfragments;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.resource;
import static android.R.attr.type;

/**
 * Created by Adriana on 3/26/2017.
 */

public class LessonAdapter extends ArrayAdapter<Lesson> {

    int resource;

    SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy");

    public LessonAdapter(Context ctx, int res, List<Lesson> lessons) {
        super(ctx, res, lessons);
        resource = res;
    }

    HashMap<String, Integer> images = new HashMap();

    private void buildMapImages() {
        images.put("Residential", R.drawable.residential);
        images.put("Commercial", R.drawable.commercial);
        images.put("Highway", R.drawable.highway);
        images.put("Clear", R.drawable.sun);
        images.put("Rainy", R.drawable.rain);
        images.put("Snowy", R.drawable.snow);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        buildMapImages();
        LinearLayout lessonView;
        Lesson lesson = getItem(position);
        if (convertView == null) {
            lessonView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
            vi.inflate(resource, lessonView, true);
        } else {
            lessonView = (LinearLayout) convertView;
        }

        TextView date = (TextView) lessonView.findViewById(R.id.dateLv);
        TextView hours = (TextView) lessonView.findViewById(R.id.hoursLv);
        date.setText(dt.format((lesson.getDate())));
        hours.setText(String.valueOf(lesson.getHours()) + " hours");

        ImageView weatherIcon = (ImageView) lessonView.findViewById(R.id.weatherIcon);
        weatherIcon.setImageResource(images.get(lesson.getWeather()));
        ImageView typeIcon = (ImageView) lessonView.findViewById(R.id.typeIcon);
        typeIcon.setImageResource(images.get(lesson.getType()));
        return lessonView;
    }
}
