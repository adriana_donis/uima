package com.cs250.joanne.myfragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import static android.R.attr.defaultValue;
import static com.cs250.joanne.myfragments.R.id.commercialSettings;
import static com.cs250.joanne.myfragments.R.id.daySettings;
import static com.cs250.joanne.myfragments.R.id.lessonTypeSpinner;
import static com.cs250.joanne.myfragments.R.id.nightSettings;
import static com.cs250.joanne.myfragments.R.id.residentialSettings;
import static com.cs250.joanne.myfragments.R.id.saveBttn;
import static com.cs250.joanne.myfragments.R.id.startBt;
import static com.cs250.joanne.myfragments.R.id.weatherSpinner;

public class SettingsActivity extends AppCompatActivity {

    String[] settings = {"daySettings", "nightSettings", "totalSettings", "residentialSettings",
            "commercialSettings", "highwaySettings", "sunnySettings", "rainySettings", "snowSettings"};
    int[] defaults = {10, 10, 20, 8, 5, 7, 8, 10, 2};
    EditText dayS, nightS, totalS, residentialS, commercialS, highwayS, sunnyS, rainyS, snowyS;
    ArrayList<EditText> setET = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPref = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle("Settings");
        initializeEditTexts();
        int j = 0;
        for (int def : defaults) {
            setET.get(j).setText(String.valueOf(sharedPref.getInt(settings[j], def)));
            j++;
        }
        Button saveBttn = (Button) findViewById(R.id.saveBttn);
        saveBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = 0;
                for (EditText et : setET) {
                    editor.putInt(settings[i], Integer.valueOf(et.getText().toString()));
                    i++;
                }
                editor.apply();
                SettingsActivity.super.onBackPressed();
            }

        });

    }

    private void initializeEditTexts() {
        dayS = (EditText) findViewById(R.id.daySettings);
        setET.add(dayS);
        nightS = (EditText) findViewById(R.id.nightSettings);
        setET.add(nightS);
        totalS = (EditText) findViewById(R.id.totalSettings);
        setET.add(totalS);
        residentialS = (EditText) findViewById(R.id.residentialSettings);
        setET.add(residentialS);
        commercialS = (EditText) findViewById(R.id.commercialSettings);
        setET.add(commercialS);
        highwayS = (EditText) findViewById(R.id.highwaySettings);
        setET.add(highwayS);
        sunnyS = (EditText) findViewById(R.id.sunnySettings);
        setET.add(sunnyS);
        rainyS = (EditText) findViewById(R.id.rainySettings);
        setET.add(rainyS);
        snowyS = (EditText) findViewById(R.id.snowSettings);
        setET.add(snowyS);
        return;
    }
}
