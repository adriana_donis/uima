package com.cs250.joanne.myfragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class LessonLog extends ListFragment {


    private ListView lessonList;
    protected static ArrayList<Lesson> lessonItems;
    protected static LessonAdapter lessonAdapter;
    private Context context;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loglesson, container, false);
        ((MainActivity) getActivity())
                .setActionBarTitle("Lesson Log");
        context = getContext();
        lessonList = (ListView) view.findViewById(R.id.lessonList);
        lessonItems = new ArrayList<Lesson>();
        lessonItems.add(new Lesson(new Date(2017, 2, 16), 0.5, "Day", "Residential", "Rainy"));
        lessonItems.add(new Lesson(new Date(2017, 2, 17), 1.5, "Night", "Commercial", "Snowy"));
        lessonItems.add(new Lesson(new Date(2017, 2, 18), 0.5, "Day", "Residential", "Rainy"));
        lessonItems.add(new Lesson(new Date(2017, 2, 19), 3, "Day", "Highway", "Clear"));
        lessonItems.add(new Lesson(new Date(2017, 2, 20), 1.5, "Day", "Residential", "Rainy"));
        lessonItems.add(new Lesson(new Date(2017, 2, 21), 2, "Night", "Highway", "Snowy"));
        lessonItems.add(new Lesson(new Date(2017, 2, 22), 5.5, "Night", "Commercial", "Clear"));
        lessonAdapter = new LessonAdapter(context, R.layout.lesson_entry, lessonItems);
        lessonList.setAdapter(lessonAdapter);

        return view;
            }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        Lesson lesson = (Lesson) lessonAdapter.getItem(position);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        ((MainActivity) getActivity()).setLessonSelected(lesson);
        transaction.replace(R.id.fragment_container, new UpdateLesson());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    // Called at the start of the visible lifetime.
    @Override
    public void onStart(){
        super.onStart();
        Log.d ("Othert Fragment", "onStart");
        // Apply any required UI change now that the Fragment is visible.
    }

    // Called at the start of the active lifetime.
    @Override
    public void onResume(){
        super.onResume();
        Log.d ("Other Fragment", "onResume");
        // Resume any paused UI updates, threads, or processes required
        // by the Fragment but suspended when it became inactive.
    }

    // Called at the end of the active lifetime.
    @Override
    public void onPause(){
        Log.d ("Other Fragment", "onPause");
        // Suspend UI updates, threads, or CPU intensive processes
        // that don't need to be updated when the Activity isn't
        // the active foreground activity.
        // Persist all edits or state changes
        // as after this call the process is likely to be killed.
        super.onPause();
    }

    // Called to save UI state changes at the
    // end of the active lifecycle.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d ("Other Fragment", "onSaveInstanceState");
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate, onCreateView, and
        // onCreateView if the parent Activity is killed and restarted.
        super.onSaveInstanceState(savedInstanceState);
    }

    // Called at the end of the visible lifetime.
    @Override
    public void onStop(){
        Log.d ("Other Fragment", "onStop");
        // Suspend remaining UI updates, threads, or processing
        // that aren't required when the Fragment isn't visible.
        super.onStop();
    }

    // Called when the Fragment's View has been detached.
    @Override
    public void onDestroyView() {
        Log.d ("Other Fragment", "onDestroyView");
        // Clean up resources related to the View.
        super.onDestroyView();
    }

    // Called at the end of the full lifetime.
    @Override
    public void onDestroy(){
        Log.d ("Other Fragment", "onDestroy");
        // Clean up any resources including ending threads,
        // closing database connections etc.
        super.onDestroy();
    }

    // Called when the Fragment has been detached from its parent Activity.
    @Override
    public void onDetach() {
        Log.d ("Other Fragment", "onDetach");
        super.onDetach();
    }
}
