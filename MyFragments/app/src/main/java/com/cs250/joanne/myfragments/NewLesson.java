package com.cs250.joanne.myfragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.DecimalFormat;
import java.util.Calendar;

import static android.R.attr.button;

public class NewLesson extends Fragment {

    private ImageView searchButton;
    long startTime;
    long endTime;
    long elapsedTime;
    DecimalFormat df = new DecimalFormat("0.00");
    double currentTotal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.newlesson_fragment, container, false);
        final TextView dateTv = (TextView) view.findViewById(R.id.dateTv);
        ImageView calendar = (ImageView) view.findViewById(R.id.calendar);
        ((MainActivity) getActivity())
                .setActionBarTitle("New Lesson");
        calendar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        String formatted = String.format("%d/%d/%d", selectedmonth, selectedday, selectedyear);
                        dateTv.setText(formatted);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }
        });


        final TextView totalHoursTv = (TextView) view.findViewById(R.id.hoursTv);
        final Spinner lessonTypeSpinner = (Spinner) view.findViewById(R.id.lessonTypeSpinner);
        final Spinner weatherSpinner = (Spinner) view.findViewById(R.id.weatherSpinner);
        final ToggleButton stopBt = (ToggleButton) view.findViewById(R.id.stopBt);
        final ToggleButton startBt = (ToggleButton) view.findViewById(R.id.startBt);
        final ToggleButton cancelBt = (ToggleButton) view.findViewById(R.id.cancelBt);
        cancelBt.setVisibility(View.GONE);
        startBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = System.nanoTime();
                totalHoursTv.setText("0.00");
                startBt.setEnabled(false);
                lessonTypeSpinner.setEnabled(false);
                weatherSpinner.setEnabled(false);
            }
        });

        stopBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime = System.nanoTime();
                elapsedTime = endTime - startTime;
                double elapsed = elapsedTime / 1000000000.0 / 3600.0;
                startBt.setTextOff(getString(R.string.start));
                startBt.setChecked(false);
                stopBt.setChecked(false);
                totalHoursTv.setText((String.valueOf(df.format(elapsed))));
                startBt.setEnabled(true);
                Toast.makeText(getActivity(), "Elapsed time: " + df.format(elapsed), Toast.LENGTH_LONG).show();
                lessonTypeSpinner.setEnabled(true);
                weatherSpinner.setEnabled(true);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
    // Called at the start of the visible lifetime.
    @Override
    public void onStart(){
        super.onStart();
        Log.d ("Othert Fragment", "onStart");
        // Apply any required UI change now that the Fragment is visible.
    }

    // Called at the start of the active lifetime.
    @Override
    public void onResume(){
        super.onResume();
        Log.d ("Other Fragment", "onResume");
        // Resume any paused UI updates, threads, or processes required
        // by the Fragment but suspended when it became inactive.
    }

    // Called at the end of the active lifetime.
    @Override
    public void onPause(){
        Log.d ("Other Fragment", "onPause");
        // Suspend UI updates, threads, or CPU intensive processes
        // that don't need to be updated when the Activity isn't
        // the active foreground activity.
        // Persist all edits or state changes
        // as after this call the process is likely to be killed.
        super.onPause();
    }

    // Called to save UI state changes at the
    // end of the active lifecycle.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d ("Other Fragment", "onSaveInstanceState");
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate, onCreateView, and
        // onCreateView if the parent Activity is killed and restarted.
        super.onSaveInstanceState(savedInstanceState);
    }

    // Called at the end of the visible lifetime.
    @Override
    public void onStop(){
        Log.d ("Other Fragment", "onStop");
        // Suspend remaining UI updates, threads, or processing
        // that aren't required when the Fragment isn't visible.
        super.onStop();
    }

    // Called when the Fragment's View has been detached.
    @Override
    public void onDestroyView() {
        Log.d ("Other Fragment", "onDestroyView");
        // Clean up resources related to the View.
        super.onDestroyView();
    }

    // Called at the end of the full lifetime.
    @Override
    public void onDestroy(){
        Log.d ("Other Fragment", "onDestroy");
        // Clean up any resources including ending threads,
        // closing database connections etc.
        super.onDestroy();
    }

    // Called when the Fragment has been detached from its parent Activity.
    @Override
    public void onDetach() {
        Log.d ("Other Fragment", "onDetach");
        super.onDetach();
    }
}
