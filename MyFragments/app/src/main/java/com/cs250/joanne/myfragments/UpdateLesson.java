package com.cs250.joanne.myfragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

import static com.cs250.joanne.myfragments.R.id.dayRb;
import static com.cs250.joanne.myfragments.R.id.hoursTv;
import static com.cs250.joanne.myfragments.R.id.nightRb;
import static com.cs250.joanne.myfragments.R.id.startBt;
import static com.cs250.joanne.myfragments.R.id.stopBt;

public class UpdateLesson extends Fragment {


    Lesson lesson;
    TextView dateTv, hoursTv;
    RadioButton dayRb, nightRb;
    Spinner weatherSp, typeSp;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.newlesson_fragment, container, false);
        dateTv = (TextView) view.findViewById(R.id.dateTv);
        hoursTv = (TextView) view.findViewById(R.id.hoursTv);
        dayRb = (RadioButton) view.findViewById(R.id.dayRb);
        nightRb = (RadioButton) view.findViewById(R.id.nightRb);
        weatherSp = (Spinner) view.findViewById(R.id.weatherSpinner);
        typeSp = (Spinner) view.findViewById(R.id.lessonTypeSpinner);

        ImageView calendar = (ImageView) view.findViewById(R.id.calendar);
        ((MainActivity) getActivity())
                .setActionBarTitle("Update Lesson");
        lesson = ((MainActivity) getActivity()).getLessonSelected();
        displayLesson();

        ToggleButton updateBttn = (ToggleButton) view.findViewById(startBt);
        updateBttn.setText("UPDATE");
        updateBttn.setTextOn("UPDATE");
        updateBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationDialog("UPDATED").show();
            }
        });
        ToggleButton cancelBttn = (ToggleButton) view.findViewById(R.id.cancelBt);
        cancelBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        ToggleButton deleteBttn = (ToggleButton) view.findViewById(stopBt);
        deleteBttn.setText("DELETE");
        deleteBttn.setTextOn("DELETE");
        deleteBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationDialog("DELETED").show();
            }
        });
        calendar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        String formatted = String.format("%d/%d/%d", selectedmonth, selectedday, selectedyear);
                        dateTv.setText(formatted);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public void displayLesson() {
        dateTv.setText(lesson.getDateString());
        hoursTv.setText(String.valueOf(lesson.getHours()));
        if (lesson.getTimeDay().equals("Day")) {
            dayRb.setChecked(true);
            nightRb.setChecked(false);
        } else {
            dayRb.setChecked(false);
            nightRb.setChecked(true);
        }
        int index = 0;
        switch(lesson.getType()) {
            case "Residential": break;
            case "Commercial": index = 1; break;
            case "Highway": index = 2; break;
        }
        typeSp.setSelection(index);

        switch(lesson.getWeather()) {
            case "Clear": index = 0; break;
            case "Rainy": index = 1; break;
            case "Snowy": index = 2; break;
        }
        weatherSp.setSelection(index);

    }
    // Called at the start of the visible lifetime.
    @Override
    public void onStart(){
        super.onStart();
        Log.d ("Othert Fragment", "onStart");
        // Apply any required UI change now that the Fragment is visible.
    }

    // Called at the start of the active lifetime.
    @Override
    public void onResume(){
        super.onResume();
        Log.d ("Other Fragment", "onResume");
        // Resume any paused UI updates, threads, or processes required
        // by the Fragment but suspended when it became inactive.
    }

    // Called at the end of the active lifetime.
    @Override
    public void onPause(){
        Log.d ("Other Fragment", "onPause");
        // Suspend UI updates, threads, or CPU intensive processes
        // that don't need to be updated when the Activity isn't
        // the active foreground activity.
        // Persist all edits or state changes
        // as after this call the process is likely to be killed.
        super.onPause();
    }

    // Called to save UI state changes at the
    // end of the active lifecycle.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d ("Other Fragment", "onSaveInstanceState");
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate, onCreateView, and
        // onCreateView if the parent Activity is killed and restarted.
        super.onSaveInstanceState(savedInstanceState);
    }

    // Called at the end of the visible lifetime.
    @Override
    public void onStop(){
        Log.d ("Other Fragment", "onStop");
        // Suspend remaining UI updates, threads, or processing
        // that aren't required when the Fragment isn't visible.
        super.onStop();
    }

    // Called when the Fragment's View has been detached.
    @Override
    public void onDestroyView() {
        Log.d ("Other Fragment", "onDestroyView");
        // Clean up resources related to the View.
        super.onDestroyView();
    }

    // Called at the end of the full lifetime.
    @Override
    public void onDestroy(){
        Log.d ("Other Fragment", "onDestroy");
        // Clean up any resources including ending threads,
        // closing database connections etc.
        super.onDestroy();
    }

    // Called when the Fragment has been detached from its parent Activity.
    @Override
    public void onDetach() {
        Log.d ("Other Fragment", "onDetach");
        super.onDetach();
    }

    public AlertDialog confirmationDialog(String message) {
        final String m = message;
        return new AlertDialog.Builder(getActivity())
                .setTitle("Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(getActivity(), m, Toast.LENGTH_SHORT).show();
                        getFragmentManager().popBackStack();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }
}
