package com.example.adriana.drivelesson;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private DatePickerDialog datePickerDialog;
    private ImageView searchButton;
    long startTime;
    long endTime;
    long elapsedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView profilePicture = (ImageView) findViewById(R.id.profilePicture);
        profilePicture.setImageResource(R.drawable.ronny_d2);
        final TextView totalHoursTv = (TextView) findViewById(R.id.hoursTv);
        final ToggleButton startBt = (ToggleButton) findViewById(R.id.startBt);
        startBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = System.nanoTime();
            }
        });
        final ToggleButton stopBt = (ToggleButton) findViewById(R.id.stopBt);
        stopBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime = System.nanoTime();
                elapsedTime = endTime - startTime;
                double elapsed = TimeUnit.HOURS.convert(elapsedTime, TimeUnit.NANOSECONDS);
                startBt.setTextOff("START");
                startBt.setChecked(false);
                stopBt.setChecked(false);
                totalHoursTv.setText("Total Hours: " + String.valueOf(elapsed));
            }
        });


    }
}
